package com.pksheldon4.embeddedkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmbeddedKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmbeddedKafkaApplication.class, args);
    }
}
